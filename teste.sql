create database if not exists teste
	default character set utf8mb4
	default collate utf8mb4_general_ci;

use teste;

create table if not exists livraria (
	id	int	not null auto_increment,
	nome	varchar(150)	not null,
	autor	varchar(150)	not null,
	primary key(id)
)engine=InnoDB auto_increment=1;

