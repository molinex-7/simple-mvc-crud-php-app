# Simple MVC CRUD PHP App

Made by Thiago Molina with technologies like Vim, PHP, MariaDB, 
Termux, and Android.

### Description

This is a simple, simple, simple, simple, simple, 1000 times simple, 
CRUD in PHP, following the MVC design pattern. That is, it is not 
so relevant.

The interesting thing here is that, my **galaxy W** bricked, after a few 
centuries of use. And a friend gave me a **lenovo**.

So I went from an **Android 2.3.6** to an **Android 6**. It was like 
traveling for the future. Although **Android 6** is already 'past' to many 
people RsRsRsRs

With this I had access to many apps that I did not have before. Among them 
the coolest was the **termux**.

Logical I had to test what it could do. So I made this simple CRUD, to see 
if it was possible to develop with it.

And yes, it is possible.

**PS:** This code has a bizarre identation. Take in mind the screen 
on which it was made.

<img src="Screenshot/Screenshot_20181126-135951.png" height="550px" width="300px">

### Run it

Imagining that you know how to install **MariaDB** and **PHP**, using the termux 
package manager pkg, first start the database server

	$ mysqld

Then enter the directory where this application is and import the database

	$ mysql -u root -p teste < teste.sql

Edit the **livraria/kernel/util/connection.class.php** file with your database user 
and password.

Finally start the internal PHP server

	$ php -S localhost:8000 -t path/to/livraria/

And go through the chrome with the URL

	http://localhost:8000

### See the result

<img src="Screenshot/Screenshot_20181126-140931.png" height="550px" width="300px" style="display: inline">
<img src="Screenshot/Screenshot_20181126-140942.png" height="550px" width="300px" style="display: inline">
<img src="Screenshot/Screenshot_20181126-140951.png" height="550px" width="300px" style="display: inline">

### Tip

Use **Termux** 

### Edit

I'm thinking of doing something in node JS in termux. Maybe Angular. So I added a Restful controller 
in that application. For when I do something in Angular, I can consume the data here.

Now, in addition to the standard output (HTML of the screenshots above), this application also 
provides input, and output data in **JSON** format. And supports the **GET**, **POST**, **PUT** and **DELETE** methods

Try it, with curl 


	$ curl -v 'http://localhost:8000/?type=livrest&action=api'
  
  
<img src="Screenshot/Screenshot_20190402-012848.png" height="550px" width="300px" style="display: inline">


### Contact

For any clarification contact us

	Mail: t.molinex@gmail.com

