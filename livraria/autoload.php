<?php

class Autoload
{
	public static function Register() {
		spl_autoload_extensions(
			'.interface.php, .class.php'
		);

		spl_autoload_register(
			function ($classes) {
				$p = str_replace(
					'\\','/',$classes
				);

				if (file_exists(
					$p.'.interface.php')) {
					require_once(
						$p.'.interface.php'
					);
				} else if (file_exists(
					$p.'.class.php')) {
					require_once(
						$p.'.class.php'
					);
				} else {
					throw new Exception(
						'Erro ao carregar classe'
					);
				}
			}
		);
	}
}

Autoload::Register();

