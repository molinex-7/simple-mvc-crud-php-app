<?php namespace kernel\controller;

use kernel\dao\livrariaDao as ld;
use kernel\model\livrariaPojo as lp;

class livrariaController
{
	private $title;
	private $action;

	private function validateId() {
		if ($_POST['id'] == "") {
			header("Location: /");
			exit;
		}
	}

	private function validate() {
		if ($_POST['nome'] == "") {
			header("Location: /");
			exit;
		}

		if ($_POST['autor'] == "") {
			header("Location: /");
			exit;
		}
	}

	private function getView($view) {
		return 'kernel/views/'.$view.'.php';
	}

	public function insert() {
		if ($_POST) {
			$l = new lp();

			$this->validate();

			$l->setNome($_POST['nome']);
			$l->setAutor($_POST['autor']);

			ld::insert($l);

			header("Location: /");
			exit;
		} else {
			$this->title = "Adicionar Livro";
			$this->action = <<<'EOT'
?type=livraria&action=insert
EOT;

			require $this->getView('form');
		}
	}

	public function edit() {
		if ($_POST) {
			$l = new lp();

			$this->validateId();
			$this->validate();

			$l->setId($_POST['id']);
			$l->setNome($_POST['nome']);
			$l->setAutor($_POST['autor']);

			ld::update($l);

			header("Location: /");
			exit;
		} else {
			$l = ld::selectOne($_GET['id']);
			$id = $l->getId();
			$nome = $l->getNome();
			$autor = $l->getAutor();
			$this->title = "Editar Livro";
			$this->action = <<<'EOT'
?type=livraria&action=edit
EOT;

			require $this->getView('form');
		}
	}

	public function delete() {
		ld::delete($_GET['id']);
		header("Location: /");
		exit;
	}

	public function lists() {
		$this->title = "Livraria";
		$objs = ld::selectAll();
		$insert = <<<'EOT'
?type=livraria&action=insert
EOT;
		$edit = <<<'EOT'
?type=livraria&action=edit&id=
EOT;
		$delete = <<<'EOT'
?type=livraria&action=delete&id=
EOT;

		require $this->getView('list');
	}
}

