<?php namespace kernel\controller;

use kernel\dao\livrariaDao as ld;
use kernel\model\livrariaPojo as lp;

class livrariaRestController
{
	public function api() {
		switch($_SERVER['REQUEST_METHOD']){ 
			case 'GET':
				if (isset($_GET['id']))
					$this->getOne($_GET['id']);
				else
					$this->getAll();

				break;
			
			case 'POST':
				$this->post();
				break;
			
			case 'PUT':
				$this->put();
				break;
			
			case 'DELETE':
				$this->delete();
				break;
		}
	}

	private function validate(
		$nome, $autor, $id
	) {
		if (
			!isset($nome) ||
			!isset($autor) ||
			!isset($id)
		) {
			header(
				$_SERVER["SERVER_PROTOCOL"]
				." 502 Bad Gateway"
			);

			exit; 
		}
	}
	
	private function getOne($id) {
		header(
			"Access-Control-Allow-Origin: *"
		);
		header(
			"Content-Type: application/json;"
			." charset=UTF-8"
		);

		$o = ld::selectOne($id);
		$arr_p = array();

		$arr_p['id'] = $o->getId();
		$arr_p['nome'] = $o->getNome();
		$arr_p['autor'] = $o->getAutor();

		echo json_encode($arr_p);
	}

	private function getAll() {
		header(
			"Access-Control-Allow-Origin: *"
		);
		header(
			"Content-Type: application/json;"
			." charset=UTF-8"
		);

		$obj = ld::selectAll();
		$arr_o = array();
		$arr_p = array();

		foreach($obj as $o) {
			$arr_p['id'] = $o->getId();
			$arr_p['nome'] = $o->getNome();
			$arr_p['autor'] = $o->getAutor();

			$arr_o[] = $arr_p;
		}

		echo json_encode($arr_o);
	}

	private function post() {
		header(
			"Access-Control-Allow-Origin: *"
		);
		header(
			"Content-Type: application/json;"
			." charset=UTF-8"
		);
		header(
			"Access-Control-Allow-Methods: "
			."POST"
		);
		header(
			"Access-Control-Max-Age: 3600"
		);
		header(
			"Access-Control-Allow-Headers:"
			." Content-Type,"
			." Access-Control-Allow-Headers,"
			." Authorization,"
			." X-Requested-With"
		);
 
		$data = json_decode(
			file_get_contents('php://input')
		);
		
		$this->validate(
			$data->nome,
			$data->autor,
			1
		);

		$l = new lp();

		$l->setNome($data->nome);
		$l->setAutor($data->autor);

		ld::insert($l);
	}

	private function put() {
		header(
			"Access-Control-Allow-Origin: *"
		);
		header(
			"Content-Type: application/json;"
			." charset=UTF-8"
		);
		header(
			"Access-Control-Allow-Methods: "
			."PUT"
		);
		header(
			"Access-Control-Max-Age: 3600"
		);
		header(
			"Access-Control-Allow-Headers:"
			." Content-Type,"
			." Access-Control-Allow-Headers,"
			." Authorization,"
			." X-Requested-With"
		);

		$data = json_decode(
			file_get_contents('php://input')
		);
		
		$this->validate(
			$data->nome,
			$data->autor,
			$data->id
		);

		$l = new lp();

		$l->setId($data->id);
		$l->setNome($data->nome);
		$l->setAutor($data->autor);

		ld::update($l);
	}

	private function delete() {
		header(
			"Access-Control-Allow-Origin: *"
		);
		header(
			"Content-Type: application/json;"
			." charset=UTF-8"
		);
		header(
			"Access-Control-Allow-Methods: "
			."DELETE"
		);
		header(
			"Access-Control-Max-Age: 3600"
		);
		header(
			"Access-Control-Allow-Headers:"
			." Content-Type,"
			." Access-Control-Allow-Headers,"
			." Authorization,"
			." X-Requested-With"
		);

		$data = json_decode(
			file_get_contents('php://input')
		);
		
		if (!isset($data->id)){
			header(
				$_SERVER["SERVER_PROTOCOL"]
				." 502 Bad Gateway"
			);

			exit; 
		}

		ld::delete($data->id);
	}
}

