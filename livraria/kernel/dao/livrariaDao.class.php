<?php namespace kernel\dao;

use kernel\util\connection as cn;
use kernel\model\livrariaPojo as lp;
use \PDO;

class livrariaDao
{
	public static $inst;
 
	public static function getInstance() {
		if (!isset(self::$inst))
			self::$inst = new livrariaDao();
 
			return self::$inst;
	}
 
	public function insert(lp $l) {
		try {
			$sql = "INSERT INTO livraria 
							VALUES (?,?,?)";       
 
			$p_sql = cn::getConn()->prepare(
				$sql
			);
 
			$p_sql->bindValue(1,0);
			$p_sql->bindValue(
				2,
				$l->getNome(),
				PDO::PARAM_STR
			);
			$p_sql->bindValue(
				3,
				$l->getAutor(),
				PDO::PARAM_STR
			);
 
			return $p_sql->execute();
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public function update(lp $l) {
		try {
			$sql = "UPDATE livraria 
							SET nome = ?,
							autor = ?
							WHERE id = ?";       
 
			$p_sql = cn::getConn()->prepare(
				$sql
			);
 
			$p_sql->bindValue(
				1,
				$l->getNome(),
				PDO::PARAM_STR
			);
			$p_sql->bindValue(
				2,
				$l->getAutor(),
				PDO::PARAM_STR
			);
			$p_sql->bindValue(
				3,
				$l->getId(),
				PDO::PARAM_INT
			);
 
			return $p_sql->execute();
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public function delete($id) {
		try {
			$sql = "DELETE FROM livraria 
							WHERE id = ?";       
 
			$p_sql = cn::getConn()->prepare(
				$sql
			);
 
			$p_sql->bindValue(1,(int)$id);
 
			return $p_sql->execute();
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public function selectOne($id) {
		try {
			$sql = "SELECT * FROM livraria 
							WHERE id = ?";       
 
			$p_sql = cn::getConn()->prepare(
				$sql
			);
 
			$p_sql->bindValue(1,(int)$id);
 
			if ($p_sql->execute()){
				if ($p_sql->rowCount() > 0) {
					$res = $p_sql->fetchObject();
					$l = new lp;
					$l->setId((int) $res->id);
					$l->setNome($res->nome);
					$l->setAutor($res->autor);

					return $l;
				}
			}

			return false;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public function selectAll() {
		try {
			$p_sql = cn::getConn()->prepare(
				"SELECT * FROM livraria 
				ORDER BY id DESC"
			);
 
			if ($p_sql->execute()){
				if ($p_sql->rowCount() > 0) {
					$arr_res = array();

					while ($res = 
						$p_sql->fetchObject()) {
						$l = new lp;
						$l->setId((int) $res->id);
						$l->setNome($res->nome);
						$l->setAutor($res->autor);
						
						$arr_res[] = $l;
					}

					return $arr_res;
				}
			}

			return false;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
}

