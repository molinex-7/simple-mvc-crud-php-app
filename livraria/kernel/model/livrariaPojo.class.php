<?php namespace kernel\model;

class livrariaPojo
{
	private $id;
	private $nome;
	private $autor;

	public function setId($id) {
		$this->id = $id;
	}

	public function getId() {
		return $this->id;
	}

	public function setNome($nome) {
		$this->nome = $nome;
	}

	public function getNome() {
		return $this->nome;
	}

	public function setAutor($autor) {
		$this->autor = $autor;
	}

	public function getAutor() {
		return $this->autor;
	}
}

