<?php 
include ('header.php');
?>
  <h2><?=$this->title; ?></h2>
	<div class="table-responsive">
		<table class="table">
			<thead class="thead-dark">
				<tr>
					<th scope="col">Livro</th>
					<th scope="col">Autor</th>
					<th scope="col"></th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody>
				<?php 
					if ($objs){
						foreach($objs as $o){
							echo '<tr><td>'
									 .$o->getNome()	
									 .'</td><td>'
									 .$o->getAutor()	
									 .'</td><td>'
									 .'<a href="'
									 .$edit
									 .$o->getId()
									 .'" class="'
									 .'btn btn-danger">'
									 .'Editar</a>'
									 .'</td><td>'
									 .'<a href="'
									 .$delete
									 .$o->getId()
									 .'" class="'
									 .'btn btn-danger">'
									 .'Apagar</a>'
									 .'</td></tr>';
						}
					}
				 ?>
			</tbody>
		</table>
	</div>
	<div class="text-center">
		<a class="btn btn-danger" 
			 href="<?=$insert; ?>">
			Inserir Novo
		</a>
	</div>
<?php 
include ('footer.php');
?>
