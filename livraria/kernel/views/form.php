<?php 
include ('header.php');
?>
  <h2><?=$this->title; ?></h2>
	<form id="myForm" 
				method="post"
				action="<?=$this->action; ?>">
    <div class="form-group">
      <label for="nome">Nome:</label>
      <input type="text" 
						 class="form-control" 
						 name="nome" 
						 value="<?=$nome; ?>"
			 			 placeholder="Entre com o nome do livro">
		</div>

    <div class="form-group">
      <label for="autor">Autor:</label>
      <input type="text" 
						 class="form-control" 
						 name="autor" 
						 value="<?=$autor; ?>"
			 			 placeholder="Entre com o nome do autor">
		</div>

		<input type="hidden" 
					 name="id" 
					 value="<?=$id; ?>">

		<div class="form-group">
			<button class="btn btn-danger" 
				type="submit">Enviar</button>
			<a href="/" 
				 class="btn btn-danger">
				 Voltar</a>
		</div>
	</form>
<?php 
include ('footer.php');
?>
