<?php namespace kernel\util;

use kernel\controller\livrariaController as lc;
use kernel\controller\livrariaRestController as lrc;

class application
{
	public static $inst;
	private static $obj;
	private static $action;
	private const default = <<<'EOT'
?type=livraria&action=lists
EOT;
 
	public static function getInstance() {
		if (!isset(self::$inst))
			self::$inst = new application();
 
			return self::$inst;
	}

	private function notFound() {
		header(
			$_SERVER["SERVER_PROTOCOL"]
			." 404 Not Found"
		);

		exit; 
	}

	private function getType() {
		$type = $_REQUEST['type'];

		if (!isset($type)) {
			header(
				"Location: http://"
				."localhost:8000/"
				.self::default
			);

			exit;
		}	

		switch ($type) {
			case 'livraria':
				self::$obj = new lc();	
				break;
			
			case 'livrest':
				self::$obj = new lrc();	
				break;
			
			default:
				self::notFound();
				break;
		}
	}

	private function getAction() {
		self::$action=$_REQUEST['action']; 

		if (method_exists(
			self::$obj,
			self::$action
		)) {
			self::$obj->{self::$action}();
		} else {
			self::notFound();
		}
	}

	public function run() {
		self::getType();
		self::getAction();
	}
}

