<?php namespace kernel\util;

use \PDO;

class connection
{
	private static $conn;

	public static function getConn(){
		if (!isset(self::$conn)) {
			self::$conn = new PDO(
		'mysql:host=localhost;dbname=teste', 
				'user', 
				'pass',
				array(
					PDO::MYSQL_ATTR_INIT_COMMAND 
					=> "SET NAMES utf8"
				)
			);

			self::$conn->setAttribute(
				PDO::ATTR_ERRMODE, 
				PDO::ERRMODE_EXCEPTION
			);

			self::$conn->setAttribute(
				PDO::ATTR_ORACLE_NULLS, 
				PDO::NULL_EMPTY_STRING
			);
		}

		return self::$conn;
	}
}

