<?php
date_default_timezone_set(
	'America/Sao_Paulo'
);

mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');

require_once 'autoload.php';

use kernel\util\application as app;

try {
	app::run();
} catch (Exception $e) {
	echo $e->getMessage();
}

